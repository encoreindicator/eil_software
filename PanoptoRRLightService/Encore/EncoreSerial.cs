﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.IO.Ports;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.ComponentModel;

namespace RRLightProgram
{
	class EncoreSerial : IInputResultReceiver
	{

		#region Variables

		/// <summary>
		/// Interface to post input events to the state machine; state machine in turn controls RR. 
		/// </summary>
		private IStateMachine stateMachine;

		/// <summary>
		/// Controller of the remote recorder; used read-only to get recording info.
		/// </summary>
		private RemoteRecorderSync remoteRecorder;


		///Timer
		private System.Timers.Timer statePingTimer;

		private bool isRecording;

		NamedPipeClientStream pipeClientOut;
		NamedPipeClientStream pipeClientIn;
		StreamReader inputReader;
		StreamWriter outputWriter;

		Queue<String> writerQueue;

		private ManualResetEvent inputQueueEvent = new ManualResetEvent(initialState: false);

		private bool isConnectedToMiddleware;

		private Thread readThreadP;
		private Thread writeThreadP;

		#endregion

		#region Constructor, Initialize, and Cleanup

		/// <summary>
		/// Constructor
		/// </summary>
		public EncoreSerial(IStateMachine stateMachine)
		{
			if (stateMachine == null)
			{
				throw new ArgumentException("stateMachine cannot be null.");
			}
			this.stateMachine = stateMachine;
		}

		/// <summary>
		/// Initialize the device and start background threads.
		/// </summary>
		/// <returns>true on success, false on failure.</returns>
		public bool Start(RemoteRecorderSync remoteRecorder)
		{
			Trace.TraceInformation("[RRLS] Program Started");

			

			writerQueue = new Queue<string>();
			readThreadP = new Thread(NamedPipeReaderThread);
			readThreadP.Start();
			writeThreadP = new Thread(NamedPipeWriterThread);
			

			/// Set Remote Recorder
			if (remoteRecorder == null)
			{
				throw new ArgumentException("remoteRecorder cannot be null.");
			}
			this.remoteRecorder = remoteRecorder;

			/// Timers
			statePingTimer = new System.Timers.Timer(60000); // Once a Minute
			statePingTimer.Elapsed += StatePingTimerEvent;
			statePingTimer.AutoReset = true;
			statePingTimer.Enabled = true;

			Trace.TraceInformation("[RRLS] Program Startup Complete");
			return true;
		}

		/// <summary>
		/// Closes the devices, stops the timers. 
		/// </summary>
		public void Stop()
		{
			Trace.TraceInformation("[RRLS] Program Closed");
			writerQueue.Enqueue("_Close_");
			Thread.Sleep(3000);

			statePingTimer.Stop();
			statePingTimer.Dispose();
		}

		#endregion

		#region IInputResultReceiver Implementation

		public void OnInputProcessed(Input input, Result result)
		{
			if (result.Equals(Result.Success))
			{
				State s = this.stateMachine.GetCurrentState();
				int time = 0;
				if (this.remoteRecorder.GetCurrentRecording() != null)
				{
					time = Convert.ToInt32((this.remoteRecorder.GetCurrentRecording().EndTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
					//writerQueue.Enqueue(time.ToString());
				}
				
				else if (this.remoteRecorder.GetNextRecordingWithinRange() != null)
				{
					time = Convert.ToInt32((this.remoteRecorder.GetNextRecordingWithinRange().StartTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
					//writerQueue.Enqueue(time.ToString());
				}

				if (input.Equals(Input.CommandExtend))
				{
					writerQueue.Enqueue("Result-CommandExtend");
				}
				else
				{
					writerQueue.Enqueue("Result-State|" + s + "|" + time);
					//writerQueue.Enqueue(s + "");
					Trace.TraceInformation("[RRLS] Result-State: " + s.ToString());
				}
				this.inputQueueEvent.Set();

				if (s.Equals(State.PotentialRecording) || s.Equals(State.Recording) || s.Equals(State.RecordingWithinEndingWindow) || s.Equals(State.TransitionAnyToRecording) || s.Equals(State.TransitionPausedToRecording) || s.Equals(State.TransitionPausedToStop) || s.Equals(State.TransitionRecordingToPause))
				{
					isRecording = true;
				}
				else
				{
					isRecording = false;
				}
			}
		}

		#endregion

		#region Timers

		private void StatePingTimerEvent(Object source, ElapsedEventArgs e)
		{
			/// Since we got here, a ping got missed or something. Assume there's summat wrong somewhere, lad. 

			if (isRecording)
			{
				Trace.TraceWarning("[RRLS] State Ping Timeout");
				this.stateMachine.PostInput(Input.CommandPause);
			}
		}

		#endregion

		#region Middleware Communicators

		private void NamedPipeReaderThread()
		{
			isConnectedToMiddleware = false;

			Trace.TraceInformation("[RRLS] Starting Pipe");

			while (true)
			{
				pipeClientIn = new NamedPipeClientStream("ENCORE_SERVICE_MAIN_PIPE_OUT");
				pipeClientOut = new NamedPipeClientStream("ENCORE_SERVICE_MAIN_PIPE_IN"); /// THIS IS BACKWARDS FROM THE OTHER PROGRAM; don't know if it's better XD
				Trace.TraceWarning("[RRLS-R] Waiting for Connection...");
				while (!pipeClientOut.IsConnected)
				{
					try
					{
						pipeClientOut.Connect();
						pipeClientIn.Connect();
					}
					catch(Exception e)
                    {
						Trace.TraceWarning("[RRLS-R] Exception caught whilst connecting - " + e.ToString());
						//Thread.Sleep(1000);
                    }
					Thread.Sleep(1000);
				}

				Trace.TraceInformation("[RRLS-R] Pipes Connected");
				//pipeClientOut.ReadMode = PipeTransmissionMode.Message;
				inputReader = new StreamReader(pipeClientIn);
				outputWriter = new StreamWriter(pipeClientOut);
				//outputWriter.AutoFlush = false;

				if (!inputReader.ReadLine().Equals("_init_"))
				{
					Trace.TraceError("[RRLS-R] Connection to Encore Service failed due to incorrect pipe response");
					throw new Exception("Encore Service Incompatible");
				}
				outputWriter.Flush();
				outputWriter.WriteLine("_init_");
				Thread.Sleep(500);
				outputWriter.Flush();

				isConnectedToMiddleware = true;
				if (!writeThreadP.IsAlive)
				{
					writeThreadP.Start();
				}

				while (isConnectedToMiddleware)
				{
					try
					{
						Trace.TraceInformation("[RRLS-R] Checking for Reads");
						string newInput = inputReader.ReadLine();
						if(newInput == null)
                        {
							isConnectedToMiddleware = false;
						}
						else if (newInput.Equals("CommandStart") || newInput.Equals("CommandStop") || newInput.Equals("CommandExtend") || newInput.Equals("CommandPause") || newInput.Equals("CommandResume"))
						{
							Input inputCommand;

							if (Enum.TryParse(newInput, true, out inputCommand))
							{
								this.stateMachine.PostInput(inputCommand);
							}
							else
							{
								Trace.TraceError("[RRLS] WARNING: Input Enum file corrupted/changed");
							}
						}
						else if (newInput.Equals("CommandOptOut"))
						{
							TraceVerbose.Trace("[RRLS] Attempting to Opt Out...");
							if (!(this.remoteRecorder.StartNextRecording() && this.remoteRecorder.StopCurrentRecording()))
							{
								Trace.TraceError("[RRLS] Opt Out Failed");
								writerQueue.Enqueue("_Service Failure_");
								this.inputQueueEvent.Set();
							}
						}
						else if (newInput.Equals("Status"))
						{
							statePingTimer.Stop();
							statePingTimer.Start();
							int time = 0;
							if (this.remoteRecorder.GetCurrentRecording() != null)
							{
								time = Convert.ToInt32((this.remoteRecorder.GetCurrentRecording().EndTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
								//writerQueue.Enqueue(time.ToString());
							}
							else if (this.remoteRecorder.GetNextRecordingWithinRange() != null)
							{ 
								time = Convert.ToInt32((this.remoteRecorder.GetNextRecordingWithinRange().StartTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
								//writerQueue.Enqueue(time.ToString());
							}

							writerQueue.Enqueue("Status-State|" + this.stateMachine.GetCurrentState().ToString() + "|" + time.ToString());
							//writerQueue.Enqueue(this.stateMachine.GetCurrentState().ToString());

							// If recording currently, send that time, else send next time, else send 20000 [placeholder]
							//if(this.remoteRecorder.GetCurrentRecording() != null)
							//{
							//	int time = Convert.ToInt32((this.remoteRecorder.GetCurrentRecording().EndTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
							//	writerQueue.Enqueue(time.ToString());
							//}
							//else if (this.remoteRecorder.GetNextRecording() == null)
							//{
							//	writerQueue.Enqueue("20000");
							//}
							//else
							//{
							//	int time = Convert.ToInt32((this.remoteRecorder.GetNextRecording().StartTime.ToLocalTime() - DateTime.Now.ToLocalTime()).TotalMinutes);
							//	writerQueue.Enqueue(time.ToString());
							//}


							this.inputQueueEvent.Set();
							// TODO RESPOND
						}
						else
						{
							Trace.TraceError("[RRLS] Unrecognised input from Encore Service: " + newInput);
						}
					}
					catch(Exception e)
                    {
						Trace.TraceError("[EIL-R] Exception Thrown in Pipe Read - " + e.ToString() + " - assuming disconnected");
					}
					//Thread.Sleep(1000);
				}
				pipeClientIn.Close();
				pipeClientOut.Close();
			}
		}

		private void NamedPipeWriterThread()
		{
			Trace.TraceInformation("[RRLS-W] Writer Thread Active");
			while (true)
			{
				WaitHandle.WaitAny(new WaitHandle[] { this.inputQueueEvent });
				while (writerQueue.Count > 0)
				{
					string output = writerQueue.Dequeue();
					Trace.TraceInformation("[RRLS-W] Making Write - " + output);
					try
					{
						outputWriter.WriteLine(output);
						//Thread.Sleep(100);
						outputWriter.Flush();

						Trace.TraceInformation("[RRLS-W] Written");
					}
					catch (Exception e)
                    {
						Trace.TraceError("[RRLS-W] Exception Caught in Write Thread: " + e.ToString());
                    }
				}
				this.inputQueueEvent.Reset();
			}
		}

		#endregion
	}
}



